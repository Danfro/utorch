v4.0.6
- updated: translations, many thanks to all translators!

v4.0.5
- updated: translations, many thanks to all translators!

v4.0.4
- updated: translations and added several languages, many thanks to all translators!

v4.0.3
- fixed: broken settings page introduced with v4.0.2

v4.0.2
- updated: translations and added several languages, many thanks to all translators!

v4.0.1
- fixed: issue with anchoring for warning message
- improved: link to README for an updated list of supported devices
- improved: link to Gitlab for contributors
- improved: use UBports Hosted Weblate for translation services, thanks to Weblate for providing us with a free service
- updated: translations, many thanks to all translators!

v4.0.0
- updated: new maintainer Daniel Frost @danfro
- updated: migrate code to GitLab
- updated: framework to 20.04
- removed: new design question
- updated: screensaver to Qt.application.state
- fixed: make about page only flick if needed
- updated: remove usage counter from about page
