﻿import QtQuick 2.0
import Lomiri.Components 1.3

Page {
    id: welcomeScreen
    visible: false
    header: PageHeader {
        visible: false
    }
    Behavior on opacity { LomiriNumberAnimation {duration: 1000} }

    Column {
        width: parent.width
        anchors.margins: units.gu(4)
        anchors.verticalCenter: parent.verticalCenter
        spacing: units.gu(5)

        LomiriShape {
            id: icon1
            anchors.horizontalCenter: parent.horizontalCenter
            width: units.gu(12)
            height: units.gu(12)
            source: Image {
                source: Qt.resolvedUrl("uTorch.png")
            }
        }

        Label {
            text: i18n.tr("To use uTorch you have to allow access to your Camera")
            width: parent.width - units.gu(4)
            anchors.horizontalCenter: parent.horizontalCenter
            fontSize: "large"
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap
        }

        Button {
            text: i18n.tr("Go to app")
            color: LomiriColors.green
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: {
                welcomeScreen.opacity = 0
                pageStack.pop()
                settings.firstStart = false
            }
        }
    }

}
