TEMPLATE = aux
TARGET = uTorch

RESOURCES += uTorch.qrc

QML_FILES += $$files(*.qml,true) \
             $$files(*.js,true)

CONF_FILES +=  uTorch.apparmor \
               uTorch.png \
               donate.svg \
               circle.png \
               slider.png \
               flash.png \
               up-arrows.png

AP_TEST_FILES += tests/autopilot/run \
                 $$files(tests/*.py,true)

OTHER_FILES += $${CONF_FILES} \
               $${QML_FILES} \
               $${AP_TEST_FILES} \
               uTorch.desktop

#specify where the qml/js files are installed to
qml_files.path = /uTorch
qml_files.files += $${QML_FILES}

#specify where the config files are installed to
config_files.path = /uTorch
config_files.files += $${CONF_FILES}

#install the desktop file, a translated version is 
#automatically created in the build directory
desktop_file.path = /uTorch
desktop_file.files = $$OUT_PWD/uTorch.desktop
desktop_file.CONFIG += no_check_exist

INSTALLS+=config_files qml_files desktop_file

DISTFILES += \
    AboutPage.qml \
    MainPage.qml \
    WelcomeScreen.qml \
    TorchButton.qml \
    TorchLever.qml \
    TorchSwitch.qml

